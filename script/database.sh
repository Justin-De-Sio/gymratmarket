#!/bin/sh

# Create database
echo "create MarketDB_test"
php bin/console doctrine:database:create --env=test

# Run migrations
echo "Running migrations..."
php bin/console doctrine:migrations:migrate --no-interaction
php bin/console doctrine:migrations:migrate --env=test --no-interaction

