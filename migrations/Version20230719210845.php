<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230719210845 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE Address (id INT AUTO_INCREMENT NOT NULL, address_line1 VARCHAR(255) NOT NULL, address_line2 VARCHAR(255) DEFAULT NULL, postal_code VARCHAR(20) NOT NULL, country VARCHAR(255) NOT NULL, city VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE Auction (id INT AUTO_INCREMENT NOT NULL, item_id INT NOT NULL, lastest_bidder_id INT DEFAULT NULL, seller_id INT NOT NULL, start_price NUMERIC(10, 0) NOT NULL, current_price NUMERIC(10, 2) DEFAULT NULL, bid_count INT DEFAULT NULL, start_time DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', end_time DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', UNIQUE INDEX UNIQ_1159CC0F126F525E (item_id), INDEX IDX_1159CC0F427C2382 (lastest_bidder_id), INDEX IDX_1159CC0F8DE820D9 (seller_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE BestOffer (id INT AUTO_INCREMENT NOT NULL, offer_price INT NOT NULL, offer_date DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', is_accepted TINYINT(1) NOT NULL, ItemID INT DEFAULT NULL, INDEX IDX_ED604BB6893A928F (ItemID), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE Category (id INT AUTO_INCREMENT NOT NULL, parent_id INT DEFAULT NULL, name VARCHAR(50) NOT NULL, INDEX IDX_FF3A7B97727ACA70 (parent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE Image (id INT AUTO_INCREMENT NOT NULL, item_id INT NOT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_4FC2B5B126F525E (item_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE Item (category_id INT NOT NULL, selling_category_id INT NOT NULL, ItemID INT AUTO_INCREMENT NOT NULL, Name VARCHAR(255) NOT NULL, Description TEXT DEFAULT NULL, Video VARCHAR(255) DEFAULT NULL, BuyItNowPrice NUMERIC(10, 2) DEFAULT NULL, IsSold TINYINT(1) DEFAULT NULL, created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', sellerID INT DEFAULT NULL, INDEX IDX_BF298A2012469DE2 (category_id), INDEX IDX_BF298A204D9B9B07 (selling_category_id), INDEX SellerID (SellerID), PRIMARY KEY(ItemID)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE Offer (OfferID INT AUTO_INCREMENT NOT NULL, OfferAmount NUMERIC(10, 2) DEFAULT NULL, OfferTime DATETIME DEFAULT NULL, IsAccepted TINYINT(1) DEFAULT NULL, ItemID INT DEFAULT NULL, BuyerID INT DEFAULT NULL, INDEX IDX_E817A83A893A928F (ItemID), INDEX IDX_E817A83AEA526F28 (BuyerID), PRIMARY KEY(OfferID)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE Payment (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, card_type VARCHAR(20) NOT NULL, card_number VARCHAR(255) NOT NULL, card_holder_name VARCHAR(255) NOT NULL, security_code VARCHAR(255) DEFAULT NULL, expiry_month INT NOT NULL, expiry_year INT NOT NULL, UNIQUE INDEX UNIQ_A295BD91A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE Purchase (PurchaseID INT AUTO_INCREMENT NOT NULL, PurchaseTime DATETIME DEFAULT CURRENT_TIMESTAMP, FinalPrice NUMERIC(10, 2) NOT NULL, ItemID INT DEFAULT NULL, BuyerID INT DEFAULT NULL, INDEX IDX_9861B36D893A928F (ItemID), INDEX IDX_9861B36DEA526F28 (BuyerID), PRIMARY KEY(PurchaseID)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE SellingCategory (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ShoppingCart (CartID INT AUTO_INCREMENT NOT NULL, BuyerID INT DEFAULT NULL, INDEX IDX_2B2F0082EA526F28 (BuyerID), PRIMARY KEY(CartID)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ShoppingCartItem (CartID INT NOT NULL, ItemID INT NOT NULL, INDEX IDX_46D935BD5E985EC2 (CartID), INDEX IDX_46D935BD893A928F (ItemID), PRIMARY KEY(CartID, ItemID)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE User (id INT AUTO_INCREMENT NOT NULL, address_id INT DEFAULT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, firstname VARCHAR(50) NOT NULL, prename VARCHAR(50) NOT NULL, phone_number VARCHAR(20) DEFAULT NULL, is_verified TINYINT(1) NOT NULL, created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', UNIQUE INDEX UNIQ_2DA17977E7927C74 (email), UNIQUE INDEX UNIQ_2DA17977F5B7AF75 (address_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE messenger_messages (id BIGINT AUTO_INCREMENT NOT NULL, body LONGTEXT NOT NULL, headers LONGTEXT NOT NULL, queue_name VARCHAR(190) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', available_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', delivered_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_75EA56E0FB7336F0 (queue_name), INDEX IDX_75EA56E0E3BD61CE (available_at), INDEX IDX_75EA56E016BA31DB (delivered_at), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE Auction ADD CONSTRAINT FK_1159CC0F126F525E FOREIGN KEY (item_id) REFERENCES Item (ItemID)');
        $this->addSql('ALTER TABLE Auction ADD CONSTRAINT FK_1159CC0F427C2382 FOREIGN KEY (lastest_bidder_id) REFERENCES User (id)');
        $this->addSql('ALTER TABLE Auction ADD CONSTRAINT FK_1159CC0F8DE820D9 FOREIGN KEY (seller_id) REFERENCES User (id)');
        $this->addSql('ALTER TABLE BestOffer ADD CONSTRAINT FK_ED604BB6893A928F FOREIGN KEY (ItemID) REFERENCES Item (ItemID)');
        $this->addSql('ALTER TABLE Category ADD CONSTRAINT FK_FF3A7B97727ACA70 FOREIGN KEY (parent_id) REFERENCES Category (id)');
        $this->addSql('ALTER TABLE Image ADD CONSTRAINT FK_4FC2B5B126F525E FOREIGN KEY (item_id) REFERENCES Item (ItemID)');
        $this->addSql('ALTER TABLE Item ADD CONSTRAINT FK_BF298A2012469DE2 FOREIGN KEY (category_id) REFERENCES Category (id)');
        $this->addSql('ALTER TABLE Item ADD CONSTRAINT FK_BF298A20B600B5FF FOREIGN KEY (sellerID) REFERENCES User (id)');
        $this->addSql('ALTER TABLE Item ADD CONSTRAINT FK_BF298A204D9B9B07 FOREIGN KEY (selling_category_id) REFERENCES SellingCategory (id)');
        $this->addSql('ALTER TABLE Offer ADD CONSTRAINT FK_E817A83A893A928F FOREIGN KEY (ItemID) REFERENCES Item (ItemID) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE Offer ADD CONSTRAINT FK_E817A83AEA526F28 FOREIGN KEY (BuyerID) REFERENCES User (id)');
        $this->addSql('ALTER TABLE Payment ADD CONSTRAINT FK_A295BD91A76ED395 FOREIGN KEY (user_id) REFERENCES User (id)');
        $this->addSql('ALTER TABLE Purchase ADD CONSTRAINT FK_9861B36D893A928F FOREIGN KEY (ItemID) REFERENCES Item (ItemID) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE Purchase ADD CONSTRAINT FK_9861B36DEA526F28 FOREIGN KEY (BuyerID) REFERENCES User (id)');
        $this->addSql('ALTER TABLE ShoppingCart ADD CONSTRAINT FK_2B2F0082EA526F28 FOREIGN KEY (BuyerID) REFERENCES User (id)');
        $this->addSql('ALTER TABLE ShoppingCartItem ADD CONSTRAINT FK_46D935BD5E985EC2 FOREIGN KEY (CartID) REFERENCES ShoppingCart (CartID)');
        $this->addSql('ALTER TABLE ShoppingCartItem ADD CONSTRAINT FK_46D935BD893A928F FOREIGN KEY (ItemID) REFERENCES Item (ItemID)');
        $this->addSql('ALTER TABLE User ADD CONSTRAINT FK_2DA17977F5B7AF75 FOREIGN KEY (address_id) REFERENCES Address (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE Auction DROP FOREIGN KEY FK_1159CC0F126F525E');
        $this->addSql('ALTER TABLE Auction DROP FOREIGN KEY FK_1159CC0F427C2382');
        $this->addSql('ALTER TABLE Auction DROP FOREIGN KEY FK_1159CC0F8DE820D9');
        $this->addSql('ALTER TABLE BestOffer DROP FOREIGN KEY FK_ED604BB6893A928F');
        $this->addSql('ALTER TABLE Category DROP FOREIGN KEY FK_FF3A7B97727ACA70');
        $this->addSql('ALTER TABLE Image DROP FOREIGN KEY FK_4FC2B5B126F525E');
        $this->addSql('ALTER TABLE Item DROP FOREIGN KEY FK_BF298A2012469DE2');
        $this->addSql('ALTER TABLE Item DROP FOREIGN KEY FK_BF298A20B600B5FF');
        $this->addSql('ALTER TABLE Item DROP FOREIGN KEY FK_BF298A204D9B9B07');
        $this->addSql('ALTER TABLE Offer DROP FOREIGN KEY FK_E817A83A893A928F');
        $this->addSql('ALTER TABLE Offer DROP FOREIGN KEY FK_E817A83AEA526F28');
        $this->addSql('ALTER TABLE Payment DROP FOREIGN KEY FK_A295BD91A76ED395');
        $this->addSql('ALTER TABLE Purchase DROP FOREIGN KEY FK_9861B36D893A928F');
        $this->addSql('ALTER TABLE Purchase DROP FOREIGN KEY FK_9861B36DEA526F28');
        $this->addSql('ALTER TABLE ShoppingCart DROP FOREIGN KEY FK_2B2F0082EA526F28');
        $this->addSql('ALTER TABLE ShoppingCartItem DROP FOREIGN KEY FK_46D935BD5E985EC2');
        $this->addSql('ALTER TABLE ShoppingCartItem DROP FOREIGN KEY FK_46D935BD893A928F');
        $this->addSql('ALTER TABLE User DROP FOREIGN KEY FK_2DA17977F5B7AF75');
        $this->addSql('DROP TABLE Address');
        $this->addSql('DROP TABLE Auction');
        $this->addSql('DROP TABLE BestOffer');
        $this->addSql('DROP TABLE Category');
        $this->addSql('DROP TABLE Image');
        $this->addSql('DROP TABLE Item');
        $this->addSql('DROP TABLE Offer');
        $this->addSql('DROP TABLE Payment');
        $this->addSql('DROP TABLE Purchase');
        $this->addSql('DROP TABLE SellingCategory');
        $this->addSql('DROP TABLE ShoppingCart');
        $this->addSql('DROP TABLE ShoppingCartItem');
        $this->addSql('DROP TABLE User');
        $this->addSql('DROP TABLE messenger_messages');
    }
}
