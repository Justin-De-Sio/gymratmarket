<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Test the availability of specific application routes.
 */
class ApplicationAvailabilityFunctionalTest extends WebTestCase
{
    /**
     * @dataProvider urlProvider
     *
     * @param string $url
     */
    public function testPageIsSuccessful($url)
    {
        $client = self::createClient();
        $client->request('GET', $url);

        $this->assertResponseIsSuccessful(sprintf('The %s route is not accessible.', $url));
    }

    /**
     * Provides URLs to the test function.
     *
     * @return \Generator
     */
    public function urlProvider()
    {
        // Public routes
        yield ['/'];
        yield ['/login'];
        yield ['/register'];
        
        yield ['/items/category'];
    }
}
