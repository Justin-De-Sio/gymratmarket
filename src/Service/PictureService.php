<?php

namespace App\Service;

use Exception;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class PictureService
{
    private ParameterBagInterface $params;

    public function __construct(ParameterBagInterface $params)
    {
        $this->params = $params;
    }

    /**
     * Adds a picture after resizing and cropping it into a square.
     *
     * @param UploadedFile $picture The picture to be added.
     * @param string|null $folder The folder to store the picture in.
     * @param int|null $width The width to resize the picture to.
     * @param int|null $height The height to resize the picture to.
     *
     * @return string The file name of the added picture.
     *
     * @throws Exception If the picture format is incorrect.
     */
    public function add(UploadedFile $picture, ?string $folder = '', ?int $width = 300, ?int $height = 300): string
    {
        $fileName = md5(uniqid(rand(), true)) . '.webp';
        $pictureInfos = getimagesize($picture);

        if($pictureInfos === false){
            throw new Exception('Incorrect image format.');
        }

        $pictureSource = $this->createImageSource($pictureInfos['mime'], $picture);
        $resizedPicture = $this->resizeImage($pictureSource, $pictureInfos, $width, $height);

        $path = $this->params->get('images_directory') . $folder;


        if(!file_exists($path . '/mini/')){
            mkdir($path . '/mini/', 0755, true);
        }

        imagewebp($resizedPicture, $path . '/mini/' . $width . 'x' . $height . '-' . $fileName);
        $picture->move($path . '/', $fileName);

        return $fileName;
    }

    /**
     * Deletes a picture.
     *
     * @param string $fileName The name of the file to be deleted.
     * @param string|null $folder The folder of the file to be deleted.
     * @param int|null $width The width of the file to be deleted.
     * @param int|null $height The height of the file to be deleted.
     *
     * @return bool Whether the file deletion was successful.
     */
    public function delete(string $fileName, ?string $folder = '', ?int $width = 300, ?int $height = 300): bool
    {
        if($fileName === 'default.webp'){
            return false;
        }

        $path = $this->params->get('images_directory') . $folder;
        $mini = $path . '/mini/' . $width . 'x' . $height . '-' . $fileName;
        $original = $path . '/' . $fileName;

        return $this->unlinkIfExists($mini) || $this->unlinkIfExists($original);
    }

    private function createImageSource(string $mime, string $picture)
    {
        switch($mime){
            case 'image/png':
                return imagecreatefrompng($picture);
            case 'image/jpeg':
                return imagecreatefromjpeg($picture);
            case 'image/webp':
                return imagecreatefromwebp($picture);
            default:
                throw new Exception('Incorrect image format.');
        }
    }

    private function resizeImage($pictureSource, array $pictureInfos, int $width, int $height)
    {
        $resizedPicture = imagecreatetruecolor($width, $height);
        [$srcX, $srcY, $squareSize] = $this->computeCropDimensions($pictureInfos);

        imagecopyresampled($resizedPicture, $pictureSource, 0, 0, $srcX, $srcY, $width, $height, $squareSize, $squareSize);
        return $resizedPicture;
    }

    private function computeCropDimensions(array $pictureInfos): array
    {
        [$imageWidth, $imageHeight] = $pictureInfos;

        switch ($imageWidth <=> $imageHeight){
            case -1: // portrait
                return [0, ($imageHeight - $imageWidth) / 2, $imageWidth];
            case 0: // square
                return [0, 0, $imageWidth];
            case 1: // landscape
                return [($imageWidth - $imageHeight) / 2, 0, $imageHeight];
            default:
                throw new Exception('Unable to compute crop dimensions.');
        }
    }


    private function unlinkIfExists(string $file): bool
    {
        if(file_exists($file)){
            unlink($file);
            return true;
        }

        return false;
    }
}
