<?php

// src/Twig/AppExtension.php
namespace App\Twig;

use App\Repository\CategoryRepository;
use App\Repository\SellingCategoryRepository;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

// Define the class AppExtension, which extends the AbstractExtension provided by Twig.
// This allows us to add custom functionality that can be used directly within our Twig templates.
class AppExtension extends AbstractExtension
{
    // Declare two private properties for the CategoryRepository and SellingCategoryRepository.
    private $categoryRepository;
    private $sellingCategoryRepository;

    // The constructor function is automatically called when a new instance of the class is created.
    // We are "injecting" our dependencies (CategoryRepository and SellingCategoryRepository) into our class through the constructor.
    // This means that Symfony will automatically pass these objects to our class when it is instantiated.
    public function __construct(CategoryRepository $categoryRepository, SellingCategoryRepository $sellingCategoryRepository)
    {
        // The passed dependencies are stored in the private properties.
        $this->categoryRepository = $categoryRepository;
        $this->sellingCategoryRepository = $sellingCategoryRepository;
    }

    // Twig will call the getFunctions method of the extension to know which functions are provided by this extension.
    // Here, we are returning an array of TwigFunction objects. Each TwigFunction represents a function that we can use in our Twig templates.
    public function getFunctions()
    {
        return [
            new TwigFunction('get_item_categories', [$this, 'getItemCategories']),
            new TwigFunction('get_selling_categories', [$this, 'getSellingCategories']),
        ];
    }

    // This function fetches all categories from the database using the CategoryRepository and returns the result.
    // This function can be called in any Twig template using the get_item_categories function.
    public function getItemCategories()
    {
        return $this->categoryRepository->findAll();
    }

    // This function fetches all selling categories from the database using the SellingCategoryRepository and returns the result.
    // This function can be called in any Twig template using the get_selling_categories function.
    public function getSellingCategories()
    {
        return $this->sellingCategoryRepository->findAll();
    }
}
