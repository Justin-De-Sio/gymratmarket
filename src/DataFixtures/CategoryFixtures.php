<?php

namespace App\DataFixtures;

use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CategoryFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // array of product categories
        $productCategories = ['Fitness equipment', 'Nutrition Sportive', 'Gym Clothing'];

        foreach ($productCategories as $key => $categoryName) {
            $category = new Category();
            $category->setName($categoryName);
            $manager->persist($category);

            // Add reference for the category to use in other fixtures
            $this->addReference('category-' . $key, $category);
        }

        $manager->flush();
    }
}
