<?php

namespace App\DataFixtures;

use App\Entity\Address;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory as FakerFactory;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends Fixture
{
    private UserPasswordHasherInterface $passwordEncoder;
    private $faker;

    public function __construct(UserPasswordHasherInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->faker = FakerFactory::create('en_UK');
    }

    public function load(ObjectManager $manager): void
    {
        $admin = $this->createAdmin();

        $manager->persist($admin);

        $seller = $this->createSeller();
        $manager->persist($seller);

        $buyer = $this->createBuyer();
        $manager->persist($buyer);


        for ($usr = 0; $usr < 10; $usr++) {
            $user = $this->createUser($usr);
            $this->addReference('user-' . $usr, $user);
            $manager->persist($user);
        }

        $manager->flush();
    }

    private function createAdmin(): User
    {
        $admin = new User();
        $admin->setEmail('admin@gymratmarket.com');
        $admin->setPrename('Mathias');
        $admin->setFirstname('Emmerich');
        $admin->setAddress($this->generateAddress());
        $admin->setPostalCode('75001');
        $admin->setPassword($this->passwordEncoder->hashPassword($admin, 'admin'));
        $admin->setRoles(['ROLE_ADMIN']);

        return $admin;
    }

    private function createSeller(): User
    {
        $seller = new User();
        $seller->setEmail('seller@gmail.com');
        $seller->setPrename('Justin');
        $seller->setFirstname('Bieber');
        $seller->setPassword($this->passwordEncoder->hashPassword($seller, 'secret'));
        $seller->setRoles(['ROLE_SELLER']);
        $this->addReference('seller', $seller);
        return $seller;
    }

    private function createBuyer(): User
    {
        $buyer = new User();
        $buyer->setEmail('buyer@gmail.com');
        $buyer->setPrename('Michael');
        $buyer->setFirstname('Jackson');
        $buyer->setPassword($this->passwordEncoder->hashPassword($buyer, 'secret'));
        $buyer->setRoles(['ROLE_BUYER']);
        $this->addReference('buyer', $buyer);
        return $buyer;

    }


    private function generateAddress(): Address
    {
        $address = new Address();
        $address->setAddressLine1($this->faker->streetAddress);
        $address->setPostalCode(str_replace(' ', '', $this->faker->postcode));
        $address->setCity($this->faker->city);
        $address->setCountry($this->faker->country);
        return $address;
    }

    private function createUser(int $index): User
    {
        $user = new User();
        $user->setEmail($this->faker->email);
        $user->setPrename($this->faker->lastName);
        $user->setFirstname($this->faker->firstName);
        $user->setAddress($this->generateAddress());
        $user->setPassword($this->passwordEncoder->hashPassword($user, 'secret'));

        if ($index % 2 == 0) {
            $user->setRoles(['ROLE_BUYER']);
        } else {
            $user->setRoles(['ROLE_SELLER']);
        }

        return $user;
    }
}
