<?php

namespace App\DataFixtures;

use App\Entity\Purchase;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class PurchaseFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
//        $purchase = new Purchase();
//        $purchase->setBuyer($this->getReference('user-2'));
//        $purchase->setItem($this->getReference('item-3'));
//        $purchase->setFinalprice('15');
//        $purchase->setPurchasetime(new \DateTime('now'));
//        $purchase->getItem()->setIssold(1);
//        $manager->persist($purchase);
//        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class,
            ItemFixtures::class
        ];
    }
}
