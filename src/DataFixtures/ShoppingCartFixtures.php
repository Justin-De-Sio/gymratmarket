<?php

namespace App\DataFixtures;

use App\Entity\ShoppingCart;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ShoppingCartFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
//        $cart = new ShoppingCart();
//        $cart->setBuyer($this->getReference('user-2'));
//        $cart->addItem($this->getReference('item-0'));
//        $cart->addItem($this->getReference('item-1'));
//        $cart->addItem($this->getReference('item-2'));
//        $manager->persist($cart);
//        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class,
            ItemFixtures::class
        ];
    }
}
