<?php

namespace App\DataFixtures;

use Faker;
use App\Entity\Payment;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class PaymentCardFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $faker = Faker\Factory::create('fr_FR');

        $paymentTypes = ['Visa', 'Mastercard', 'American Express', 'Payment', 'Bitcoin'];
        for ($i = 0; $i < 3; $i++) {
            $user = $this->getReference('user-' . $i);
            $payment = new Payment();
            $payment->setCardNumber($faker->creditCardNumber());
            $payment->setCardHolderName($faker->name);

            $payment->setExpiryMonth($faker->numberBetween(1, 12));
            $payment->setExpiryYear($faker->numberBetween(2024, 2030));
            $payment->setSecuritycode((string)(random_int(100, 999)));
            $payment->setCardtype($paymentTypes[$i]);
            $payment->setUser($user);
            $manager->persist($payment);
        }


        $manager->flush();
    }


    public function getDependencies()
    {
        return [
            UserFixtures::class,
        ];
    }
}
