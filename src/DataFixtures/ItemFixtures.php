<?php

namespace App\DataFixtures;

use App\Entity\Auction;
use App\Entity\Bid;
use App\Entity\Image;
use App\Entity\Item;
use App\Entity\Offer;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ItemFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        // Let's assume these are the names of our items
        $itemNames = ['kayak', 'Jump rope', 'kettle', 'Rack', 'Item 5', 'Item 6', 'Item 7', 'Item 8', 'Item 9', 'Item 10'];

        $item = new Item();
        $item->setName('kayak');
        $item->setDescription('Kayaking is an exciting water sport that allows you to navigate rivers, lakes, and even ocean waves while enjoying the beauty of nature. In this lesson, we will delve into the world of kayaking, discussing its equipment, benefits, and techniques.');
        $item->setBuyItNowPrice(300); // random buy it now price between 20 and 200
        $item->setIsSold(false);
        $item->setVideo('video_' . uniqid() . 'kayak' . 'mp4');
        $item->setCategory($this->getReference('category-0'));
        $item->setSeller($this->getReference('seller'));
        $sellingCategory = $this->getReference('selling-category-0');
        $item->setSellingCategory($sellingCategory);

        $imagesName = ['230be3aec5eaef04d961e17d00bad79c.webp', 'ef962488e6ea2e1625510e8fbd815d9b.webp', '8201728d608aa56935608cc626d52fdf.webp'];


        foreach ($imagesName as $imageName) {
            $image = new Image();
            $image->setName($imageName);
            $item->addImage($image);
        }
        $manager->persist($item);

        $item = new Item();
        $item->setName('Jump rope');
        $item->setDescription('Jump rope, also known as skipping rope, is a popular cardiovascular exercise that combines fun, simplicity, and effectiveness. In this lesson, we will explore the world of jump rope, discussing its benefits, equipment, and various techniques.');
        $item->setBuyItNowPrice(30); // random buy it now price between 20 and 200
        $item->setIsSold(false);
        $item->setVideo('video_' . uniqid() . 'Jump rope' . 'mp4');
        $item->setCategory($this->getReference('category-0'));
        $item->setSeller($this->getReference('seller'));
        $sellingCategory = $this->getReference('selling-category-0');
        $item->setSellingCategory($sellingCategory);

        $imagesName = ['aeb5a888aacb4c467e7b57515e14a91f.webp', 'cb784b5c53ea6baf463bbaa646fca9af.webp', '5ac736f6ed94ffca09f5fea0ad275166.webp', 'ba3a27ad79bab7e79e2ec46ed19a8d71.webp'];

        foreach ($imagesName as $imageName) {
            $image = new Image();
            $image->setName($imageName);
            $item->addImage($image);
        }
        $manager->persist($item);


        $item = new Item();
        $item->setName('fast shoes');
        $item->setDescription('Nike running shoes have long been a favorite choice among runners of all levels, renowned for their innovative design, comfort, and performance-enhancing features. In this lesson, we will dive into the world of Nike running shoes, discussing their benefits, technologies, and considerations for finding the perfect pair.');
        $item->setBuyItNowPrice(122);
        $item->setIsSold(false);
        $item->setVideo('video_' . uniqid() . 'fast shoes' . 'mp4');
        $item->setCategory($this->getReference('category-2'));
        $item->setSeller($this->getReference('seller'));
        $sellingCategory = $this->getReference('selling-category-0');
        $item->setSellingCategory($sellingCategory);
        $imagesName = ['8d11c2c26e2a4432de5f1a106584c246.webp', 'c0be65f4c587e95c8ad839eb9290ccc5.webp', 'c2b5633869714cb358bd2b5b37c1db70.webp'];

        foreach ($imagesName as $imageName) {
            $image = new Image();
            $image->setName($imageName);
            $item->addImage($image);
        }

        $manager->persist($item);


        $item = new Item();
        $item->setName('Pancake');
        $item->setDescription('Pancakes are a popular breakfast food that are easy to make and delicious to eat. In this lesson, we will explore the world of pancakes, discussing their history, ingredients, and various cooking methods.');
        $item->setBuyItNowPrice(20);
        $item->setIsSold(false);
        $item->setVideo('video_' . uniqid() . 'Pancake' . 'mp4');
        $item->setCategory($this->getReference('category-1'));
        $item->setSeller($this->getReference('seller'));
        $sellingCategory = $this->getReference('selling-category-0');
        $item->setSellingCategory($sellingCategory);

        $imagesName = ['313c55c74e92c92f1930945fd49905a0.webp', '465e7c9b9d315f070c21887b385db490.webp', 'd4dd99205db9c164a12365ac6caefb78.webp'];

        foreach ($imagesName as $imageName) {
            $image = new Image();
            $image->setName($imageName);
            $item->addImage($image);
        }

        $manager->persist($item);



        // Loop through item names

//        foreach ($itemNames as $key => $itemName) {
//            $item = new Item();
//            $item->setName($itemName);
//            $item->setDescription('This is a description for ' . $itemName);
//            $item->setBuyItNowPrice(rand(20, 200)); // random buy it now price between 20 and 200
//            $item->setIsSold(0);
//            $item->setVideo('video_' . uniqid() . $itemName . 'mp4');
//            $category = $this->getReference('category-' . ($key % 3));
//            $item->setSeller($this->getReference('user-' . ($key % 2))); // getting user reference
//            $item->setCategory($category); // getting category reference
//            $imageEmpty = new Image();
//            $imageEmpty->setName('07b63d0d7ab97ddedddd9b8d44b538e1.webp');
//            $item->addImage($imageEmpty);
//            if ($key % 2 == 0) { // for even keys, set sale method to Bids
//                // Add bids to the item
//                $sellingCategory = $this->getReference('selling-category-1');
//                $item->setSellingCategory($sellingCategory);
//                $auction = new Auction();
//                $auction->setStartPrice(10, 100); // random bid amount between 10 and 100
//                $auction->setItem($item);
//                $auction->setStartTime(new \DateTimeImmutable());
//                $auction->setEndTime(new \DateTimeImmutable('+1 week'));
//                $auction->setSeller($this->getReference('user-' . ($key % 2))); // getting user reference
//                $manager->persist($auction);
//            } else if ($key % 3 == 0) { // for keys divisible by 3, set sale method to Buy it now
//                $sellingCategory = $this->getReference('selling-category-0');
//                $item->setSellingCategory($sellingCategory);
//            } else { // for other keys, set sale method to Best offer
//                // Add best offers to the item
//                for ($i = 0; $i < 2; $i++) {
//                    $offer = new Offer();
//                    $sellingCategory = $this->getReference('selling-category-2');
//                    $item->setSellingCategory($sellingCategory);
//
//
//                    $initialOffer = $item->getBuyitnowprice() * (1 + $i / 10);
//                    $offer->setOfferamount($initialOffer - ($item->getBuyitnowprice() * 0.05)); // random offer amount between 10 and 100
//                    $offer->setBuyer($this->getReference('user-2')); // getting user reference
//                    $offer->setItem($item);
//                    $offer->setOffertime(new \DateTime('+' . (1 + $i) . ' hour'));
//                    $manager->persist($offer);
//                }
//            }
//
//            $manager->persist($item);
//
//            $this->addReference('item-' . $key, $item);
//        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class,
            CategoryFixtures::class,
            SellingCategoryFixtures::class
        ];
    }
}
