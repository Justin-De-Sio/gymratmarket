<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\SellingCategory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class SellingCategoryFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // array of product categories
        $sellingCategory = ['Buy It Now', 'Auction', 'Best Offer'];

        foreach ($sellingCategory as $key => $categoryName) {
            $category = new SellingCategory();
            $category->setName($categoryName);
            $manager->persist($category);
            // Add reference for the category to use in other fixtures
            $this->addReference('selling-category-' . $key, $category);
        }

        $manager->flush();
    }
}
