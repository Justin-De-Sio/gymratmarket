<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;

class EmailController extends AbstractController
{
    #[Route('send/')]
    public function sendEmail(MailerInterface $mailer)
    {
        $email = (new Email())
            ->from('sales@app.com')
            ->to('sales@app.com')
            ->subject('New Order')
            ->html('<p>You have a new order</p>');
        $mailer->send($email);

        return new Response('Email sent');
    }
}