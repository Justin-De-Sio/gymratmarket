<?php

namespace App\Controller;

use App\Repository\CategoryRepository;

use App\Repository\SellingCategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class HomeController extends AbstractController
{
    #[Route('/', name: 'app_home')]
    public function index(CategoryRepository $categoryRepository, SellingCategoryRepository $sellingCategoryRepository): Response
    {
        //get Category with repo
        $itemCategories = $categoryRepository->findAll();
        $sellingCategories = $sellingCategoryRepository->findAll();

        return $this-> render('home/index.html.twig', [

        ]);
    }

}
