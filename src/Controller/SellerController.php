<?php

namespace App\Controller;

use App\Entity\Image;
use App\Entity\Item;
use App\Entity\Purchase;
use App\Form\ItemType;
use App\Service\PictureService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[IsGranted('ROLE_SELLER')]
class SellerController extends AbstractController
{
    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    #[Route('/seller', name: 'app_seller')]
    public function seller(): Response
    {
        $items = $this->getUser()->getItems();

        return $this->render('seller/index.html.twig', [
            'items' => $items,
        ]);
    }
    #[Route('/seller/items', name: 'app_seller_items')]
    public function items(): Response
    {
        $items = $this->getUser()->getItems();
        return $this->render('admin/items/index.html.twig', [
            'controller_name' => 'AdminController',
            'items' => $items,
        ]);
    }
    #[Route('/seller/add', name: 'app_seller_add')]
    public function index(Request $request, PictureService $pictureService): Response
    {
        $item = new Item();
        $form = $this->createForm(ItemType::class, $item);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $images = $form->get('images')->getData();
            foreach ($images as $image) {
                $folder = 'items';
                $file = $pictureService->add($image, $folder, 300, 300);
                $image = new Image();
                $image->setName($file);
                $item->addImage($image);

            }

            $item->setSeller($this->getUser());
            $this->entityManager->persist($item);
            $this->entityManager->flush();
            $this->addFlash('success', 'Item added successfully');
            return $this->redirectToRoute('app_seller', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('seller/add.html.twig', [
            'form' => $form,
        ]);
    }

    //delete item standard request

    #[Route('/seller/edit/{id}', name: 'app_seller_edit')]
    public function edit(Request $request, Item $item, PictureService $pictureService): Response
    {

        if ($item->getSeller() !== $this->getUser() && !$this->isGranted('ROLE_ADMIN')) {
            throw $this->createAccessDeniedException('You are not allowed to edit this item.');
        }

        $form = $this->createForm(ItemType::class, $item);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $images = $form->get('images')->getData();
            foreach ($images as $image) {
                $folder = 'items';
                $file = $pictureService->add($image, $folder, 300, 300);
                $image = new Image();
                $image->setName($file);
                $item->addImage($image);
            }
            $this->entityManager->persist($item);
            $this->entityManager->flush();

            return $this->redirectToRoute('app_seller', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('seller/edit.html.twig', [
            'form' => $form,
            'item' => $item
        ]);
    }

    #[Route('/seller/delete/image/{id}', name: 'app_seller_delete_image', methods: "DELETE")]
    public function deleteImage(Image $image, PictureService $pictureService, EntityManagerInterface $entityManager, Request $request): Response
    {
        if ($image->getItem()->getSeller() !== $this->getUser() && !$this->isGranted('ROLE_ADMIN')) {
            throw $this->createAccessDeniedException('You are not allowed to delete this image.');
        }

        $data = json_decode($request->getContent(), true);

        if ($this->isCsrfTokenValid('delete' . $image->getId(), $data['_token'])) {
            if ($pictureService->delete($image->getName(), 'items')) {
                $entityManager->remove($image);
                $entityManager->flush();
                return new JsonResponse(['success' => 1]);
            }
            return new JsonResponse(['error' => 'delete error'], 400);
        }
        return new JsonResponse(['error' => 'invalid token'], 400);
    }

    #[Route('/seller/delete/{id}', name: 'app_seller_delete')]
    public function delete(Item $item, PictureService $pictureService): Response
    {
        if ($item->getSeller() !== $this->getUser() && !$this->isGranted('ROLE_ADMIN')) {
            throw $this->createAccessDeniedException('You are not allowed to delete this item.');
        }

        foreach ($item->getImages() as $image) {
            $pictureService->delete($image->getName(), 'items');
        }

        if ($item->isIssold()) {
            throw $this->createAccessDeniedException("You are can't delete this item because it is sold.");
        }

        $this->entityManager->remove($item);
        $this->entityManager->flush();

        return $this->redirectToRoute('app_seller', [], Response::HTTP_SEE_OTHER);
    }

}
