<?php

namespace App\Controller;

use App\Repository\ItemRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ItemController extends AbstractController


{
    public function __construct(private ItemRepository $itemRepository)
    {
    }

    #[Route('/items/category/{id?}', name: 'app_item_category')]
    public function category(?string $id = null): Response
    {
        if ($id) {
            $items = $this->itemRepository->findByCategory($id);
        } else {
            $items = $this->itemRepository->findAll();
        }

        return $this->render('item/index.html.twig', [
            'items' => $items,
        ]);
    }

    #[Route('/items/selling-category/{id?}', name: 'app_item_selling_category')]
    public function selling(?string $id = null): Response
    {
        if ($id) {
            $items = $this->itemRepository->findBySellingCategory($id);
        } else {
            $items = $this->itemRepository->findAllNotSold();
        }
        return $this->render('item/index.html.twig', [
            'items' => $items,
        ]);
    }


    #[Route('/items/{id}', name: 'app_item_detail')]
    public function detail(string $id): Response
    {
        $item = $this->itemRepository->find($id);

        if (!$item) {
            throw $this->createNotFoundException('The item does not exist');
        }

        return $this->render('item/detail.html.twig', [
            'item' => $item,
        ]);
    }
}
