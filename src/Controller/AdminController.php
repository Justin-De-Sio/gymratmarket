<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\AccountType;
use App\Repository\ItemRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[IsGranted('ROLE_ADMIN')]
class AdminController extends AbstractController
{
    public function __construct(private ItemRepository $itemRepository,private UserRepository $userRepository, private EntityManagerInterface $entityManager)
    {
    }

    #[Route('/admin', name: 'app_admin')]
    public function index(): Response
    {
        return $this->render('admin/index.html.twig', [
            'controller_name' => 'AdminController',
        ]);
    }

    #[Route('/admin/users', name: 'app_admin_users')]
    public function users(): Response
    {
        $users = $this->userRepository->findAll();
        return $this->render('admin/users/index.html.twig', [
            'users' => $users,
        ]);
    }
    #[Route('/admin/statistics', name: 'app_admin_statistics')]
    public function statistics(): Response
    {
        $items = $this->itemRepository->countByItemSold();
        dump($items);
        return $this->render('admin/statistics/index.html.twig', [
            'controller_name' => 'AdminController',
        ]);
    }

    #[Route('/admin/users/add')]
    public function addUser(Request $request): Response
    {
        return $this->redirectToRoute('app_register', [], Response::HTTP_SEE_OTHER);
    }


    #[Route('/admin/users/edit/{id}', name: 'app_admin_users_edit')]
    public function editUser(User $user, Request $request): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $form = $this->createForm(AccountType::class, $user);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->flush();
            return $this->redirectToRoute('app_admin_users', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('account/index.html.twig', [
            'user' => $user,
            'form' => $form,
        ]);
    }


    #[Route('/admin/users/delete/{id}', name: 'app_admin_users_delete')]
    public function deleteSeller(User $user): Response
    {
        foreach ($user->getItems() as $item) {
            $item->setSeller(null);
        }

        $this->userRepository->remove($user);
        $this->entityManager->flush();

        $this->addFlash('success', 'Seller deleted successfully');
        return $this->redirectToRoute('app_admin_users');
    }

    #[Route('/admin/users/add', name: 'app_admin_users_add')]
    public function addSeller(): Response
    {
        return $this->redirectToRoute('app_register');
    }

    #[Route('/admin/items', name: 'app_admin_items')]
    public function items(ItemRepository $itemRepository): Response
    {
        $items = $itemRepository->findAll();
        return $this->render('admin/items/index.html.twig', [
            'controller_name' => 'AdminController',
            'items' => $items,
        ]);
    }


}
