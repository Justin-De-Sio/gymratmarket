<?php

namespace App\Controller;

use App\Entity\Address;
use App\Entity\Payment;
use App\Entity\Purchase;
use App\Entity\ShoppingCart;
use App\Form\AddressType;
use App\Form\PaymentType;
use App\Repository\ItemRepository;
use App\Repository\ShoppingCartRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[IsGranted('ROLE_BUYER')]
class CartController extends AbstractController
{
    public function __construct(private ItemRepository $itemRepository, private ShoppingCartRepository $cartRepository, private EntityManagerInterface $entityManager)
    {
    }

    #[Route('/cart', name: 'app_cart')]
    public function index(): Response
    {
        $cart = $this->getCartForCurrentUser();
        $total = $this->getTotalCartAmount($cart);
        $items = $cart ? $cart->getItems() : null;

        return $this->render('cart/index.html.twig', [
            'controller_name' => 'CartController',
            'items' => $items,
            'total' => $total
        ]);
    }



    private function getCartForCurrentUser(bool $createIfNotExist = false): ?ShoppingCart
    {
        $cart = $this->cartRepository->findOneByBuyer($this->getUser());
        if (!$cart && $createIfNotExist) {
            $cart = new ShoppingCart();
            $cart->setBuyer($this->getUser());
            $this->entityManager->persist($cart);
        }
        return $cart;
    }

    private function getTotalCartAmount(?ShoppingCart $cart): float
    {
        $total = 0;
        if ($cart) {
            foreach ($cart->getItems() as $item) {
                $total += $item->getBuyItNowPrice();
            }
        }
        return $total;
    }

    #[Route('/cart/add/{id}', name: 'app_cart_add')]
    public function add(string $id): Response
    {
        $item = $this->itemRepository->find($id);
        if (!$item) throw $this->createNotFoundException('The item does not exist');

        $cart = $this->getCartForCurrentUser(true);
        if ($cart->getItems()->contains($item)) throw $this->createNotFoundException('The item is already in the cart');

        $cart->addItem($item);
        $this->entityManager->flush();

        return $this->redirectToRoute('app_cart');
    }

    #[Route('/cart/checkout/fill-address', name: 'app_cart_checkout_fill_address')]
    public function checkout(Request $request): Response
    {

        $user = $this->getUser();

        if (!$user->getAddress()) {
            $address = new Address();
            $user->setAddress($address);
        } else {
            $address = $user->getAddress();
        }
        $addressForm = $this->createForm(AddressType::class, $address);
        $addressForm->handleRequest($request);

        if ($addressForm->isSubmitted() && $addressForm->isValid()) {
            $this->entityManager->flush();
            return $this->redirectToRoute('app_cart_checkout_fill_payment');
        }

        return $this->render('cart/checkout_fill_address.html.twig', ['addressForm' => $addressForm]);
    }

    #[Route('/cart/checkout/fill-payment', name: 'app_cart_checkout_fill_payment')]
    public function checkoutPayment(Request $request): Response
    {
        $cart = $this->getCartForCurrentUser();
        $user = $this->getUser();
        if (!$user->getAddress()) return $this->redirectToRoute('app_cart_checkout_fill_address');

        if (!$user->getPayment()) {
            $payment = new Payment();
            $user->setPayment($payment);
        }else{
            $payment = $user->getPayment();
        }
        $paymentForm = $this->createForm(PaymentType::class, $payment);
        $paymentForm->handleRequest($request);

        if ($paymentForm->isSubmitted() && $paymentForm->isValid()) {

            $this->entityManager->flush();
            return $this->redirectToRoute('app_cart_checkout_confirm');
        }

        $total = $this->getTotalCartAmount($cart);

        return $this->render('cart/checkout_fill_payment.html.twig', [
            'paymentForm' => $paymentForm,
            'total' => $total
        ]);
    }


    #[Route('/cart/checkout/confirm', name: 'app_cart_checkout_confirm')]
    public function confirm(): Response
    {

        $user = $this->getUser();

        if (!$user->getAddress() || !$user->getPayment()) {
            return $this->redirectToRoute('app_cart_checkout_fill_payment');
        }

        $cart = $this->getCartForCurrentUser();

        $items = $cart->getItems();
        $total = 0;
        foreach ($items as $item) {
            $total += $item->getBuyitnowprice();
            $item->setIssold(1);

            $purchase = new Purchase();
            $purchase->setBuyer($user);
            $purchase->setItem($item);
            $purchase->setFinalprice($item->getBuyitnowprice());
            $this->entityManager->persist($purchase);
        }
        $this->entityManager->flush();

        $this->clearCart($cart);

        return $this->render('cart/confirm.html.twig', [
            'items' => $items,
            'total' => $total
        ]);
    }

    private function clearCart(?ShoppingCart $cart): void
    {
        if ($cart) {
            $this->entityManager->remove($cart);
            $this->entityManager->flush();
        }
    }

    #[Route('/cart/clear', name: 'app_cart_clear')]
    public function clear(): Response
    {
        $cart = $this->getCartForCurrentUser();
        $this->clearCart($cart);
        return $this->redirectToRoute('app_cart');
    }
}
