<?php

namespace App\Form;

use App\Entity\Item;
use App\Entity\SellingCategory;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ItemType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, ['label' => 'Item Name'])
            ->add('description', TextType::class, ['label' => 'Item Description'])
            ->add('Category', EntityType::class, [
                'class' => 'App\Entity\Category',
                'choice_label' => 'name',
            ])
            ->add('sellingCategory', EntityType::class, [
                'label' => 'Sale Mode',
                'class' => SellingCategory::class,
                'choice_label' => 'name',
                'attr' => ['class' => 'selling-category-selector']
            ])
            ->add('buyItNowPrice', MoneyType::class, [
                'label' => 'Buy It Now Price',
                'required' => false,
                'attr' => ['class' => 'buy-it-now-price-field']
            ])
            ->add('images', FileType::class, [
                'label' => 'Item Image',
                'required' => true,
                'multiple' => true,
                'mapped' => false,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Item::class,
        ]);
    }
}
