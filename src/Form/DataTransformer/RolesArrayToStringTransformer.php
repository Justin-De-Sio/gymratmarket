<?php

namespace App\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;

class RolesArrayToStringTransformer implements DataTransformerInterface
{
    public function transform($rolesArray)
    {
// transform the array to a string
        return implode(', ', $rolesArray);
    }

    public function reverseTransform($rolesString)
    {
// transform the string back to an array
        return explode(', ', $rolesString);
    }
}
