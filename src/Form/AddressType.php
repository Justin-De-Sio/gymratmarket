<?php

namespace App\Form;

use App\Entity\Address;
use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class AddressType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('AddressLine1', TextType::class, [
                'label' => 'Address Line 1',
                'constraints' => [
                    new NotBlank(),
                    new Length(['min' => 5])
                ],
                'attr' => ['placeholder' => 'Enter your address line 1'],
            ])
            ->add('AddressLine2', TextType::class, [
                'label' => 'Address Line 2',
                'attr' => ['placeholder' => 'Enter your address line 2 (optional)'],
                'required' => false,
            ])
            ->add('City', TextType::class, [
                'label' => 'City',
                'constraints' => new NotBlank(),
                'attr' => ['placeholder' => 'Enter your city'],
            ])
            ->add('PostalCode', TextType::class, [
                'label' => 'Postal Code',
                'constraints' => [
                    new NotBlank(),
                    new Length(['min' => 5, 'max' => 10])
                ],
                'attr' => ['placeholder' => 'Enter your postal code'],
            ])
            ->add('Country', TextType::class, [
                'label' => 'Country',
                'constraints' => new NotBlank(),
                'attr' => ['placeholder' => 'Enter your country'],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Address::class,
        ]);
    }
}
