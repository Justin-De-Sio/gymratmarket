<?php

namespace App\Form;

use App\Entity\Payment;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Validator\Constraints\Range;

class PaymentType extends AbstractType
{
    private const CARD_TYPES = [
        'Visa' => 'Visa',
        'MasterCard' => 'MasterCard',
        'American Express' => 'American Express',
    ];

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('cardType', ChoiceType::class, [
                'choices' => self::CARD_TYPES,
                'constraints' => [
                    new NotBlank(['message' => 'Please select a card type.']),
                ],
                'placeholder' => 'Choose your card',
                'label' => 'Card Type'
            ])
            ->add('cardNumber', TextType::class, [
                'constraints' => [
                    new NotBlank(['message' => 'Please provide a card number.']),
                    new Length(['min' => 16, 'max' => 16, 'exactMessage' => 'Card number should be exactly 16 digits.']),
                ],
                'label' => 'Card Number'
            ])
            ->add('cardHolderName', TextType::class, [
                'constraints' => [
                    new NotBlank(['message' => 'Please provide the card holder name.']),
                ],
                'label' => 'Card Holder Name'
            ])
            ->add('expiryMonth', ChoiceType::class, [
                'choices' => array_combine(range(1, 12), range(1, 12)),
                'constraints' => [
                    new NotBlank(),
                ],
                'placeholder' => 'Select a month',
                'label' => 'Expiry Month'
            ])
            ->add('expiryYear', ChoiceType::class, [
                'choices' => array_combine(range(date('Y'), date('Y') + 10), range(date('Y'), date('Y') + 10)),
                'constraints' => [
                    new NotBlank(),
                ],
                'placeholder' => 'Select a year',
                'label' => 'Expiry Year'
            ]);
        // We do NOT ask for the CVV security code here, it's a security risk
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Payment::class,
        ]);
    }
}
