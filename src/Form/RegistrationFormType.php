<?php

namespace App\Form;

use App\Entity\User;
use App\Form\DataTransformer\RolesArrayToStringTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\EqualTo;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class RegistrationFormType extends AbstractType
{
    private $transformer;

    public function __construct(RolesArrayToStringTransformer $transformer)
    {
        $this->transformer = $transformer;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email', EmailType::class, [
                'constraints' => [
                    new Email(['message' => 'Please enter a valid email address.']),
                    new NotBlank(['message' => 'Please enter an email address']),
                ],
                'attr' => ['placeholder' => 'Email Address'],
            ])
            ->add('firstname', TextType::class, [
                'constraints' => [
                    new NotBlank(['message' => 'Please enter your first name']),
                ],
                'attr' => ['placeholder' => 'First Name'],
            ])
            ->add('prename', TextType::class, [
                'constraints' => [
                    new NotBlank(['message' => 'Please enter your prename']),
                ],
                'attr' => ['placeholder' => 'Prename'],
            ])
            ->add('roles', ChoiceType::class, [
                'choices' => [
                    'Buyer' => 'ROLE_BUYER',
                    'Seller' => 'ROLE_SELLER',
                ],
                'constraints' => [
                    new NotBlank(['message' => 'Please select a role']),
                ],
                'multiple' => false,
                'expanded' => true, // Set to true for radio buttons
            ])
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'mapped' => false,
                'invalid_message' => 'The password fields must match.',
                'options' => ['attr' => ['autocomplete' => 'new-password', 'placeholder' => 'Password']],
                'required' => true,
                'first_options' => ['label' => 'Password'],
                'second_options' => ['label' => 'Repeat Password'],
                'constraints' => [
                    new NotBlank(['message' => 'Please enter a password']),
                    new Length([
                        'min' => 6,
                        'minMessage' => 'Your password should be at least {{ limit }} characters',
                        'max' => 4096,
                    ]),
                ],
                'help' => 'Your password should be at least 6 characters long',
            ])->add('agreeTerms', CheckboxType::class, [
                'mapped' => false,
                'constraints' => [
                    new IsTrue(['message' => 'You should agree to our terms.']),
                ],
                'help' => 'Read and agree to the terms and conditions before proceeding',
            ]);


        $builder->get('roles')->addModelTransformer($this->transformer);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
