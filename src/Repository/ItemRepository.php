<?php


namespace App\Repository;

use App\Entity\Item;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;


/**
 *
 * @extends ServiceEntityRepository<Item>
 *
 * @method Item|null find($id, $lockMode = null, $lockVersion = null)
 * @method Item|null findOneBy(array $criteria, array $orderBy = null)
 * @method Item[]    findAll()
 * @method Item[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Item::class);
    }


    public function findByCategory(string $id): array
    {
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            'SELECT i
            FROM App\Entity\Item i
            WHERE i.Category = :id'
        )->setParameter('id', $id);

        // returns an array of Product objects
        return $query->getResult();
    }

    public function findBySellingCategory(string $id): array
    {
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            'SELECT i
            FROM App\Entity\Item i
            WHERE i.sellingCategory = :id AND i.issold = 0'
        )->setParameter('id', $id);

        // returns an array of Product objects
        return $query->getResult();
    }

    public function findAllNotSold(): array
    {
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            'SELECT i
            FROM App\Entity\Item i
            WHERE i.issold = 0'
        );

        return $query->getResult();
    }

    public function countByItemSold(): int
    {
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            'SELECT COUNT(i)
            FROM App\Entity\Item i
            WHERE i.issold = 1'
        );

        return $query->getSingleScalarResult();
    }
}
