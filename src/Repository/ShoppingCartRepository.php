<?php

namespace App\Repository;

use App\Entity\Item;
use App\Entity\ShoppingCart;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 *
 * @extends ServiceEntityRepository<ShoppingCart>
 *
 * @method Item|null find($id, $lockMode = null, $lockVersion = null)
 * @method Item|null findOneBy(array $criteria, array $orderBy = null)
 * @method Item[]    findAll()
 * @method Item[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ShoppingCartRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ShoppingCart::class);
    }


    public function findOneByBuyer(User $buyer): ?ShoppingCart
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.buyer = :buyer')
            ->setParameter('buyer', $buyer)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function getTotal(User $user): float
    {
        return $this->createQueryBuilder('c')
            ->select('SUM(i.buyItNowPrice)')
            ->innerJoin('c.items', 'i')
            ->where('c.buyer = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->getSingleScalarResult();
    }


}
