<?php

namespace App\Entity;

use App\Repository\ItemRepository;
use App\Entity\Traits\CreateAtTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ItemRepository::class)]
#[ORM\Table(name: "Item")]
#[ORM\Index(name: "SellerID", columns: ["SellerID"])]
class Item
{
    use CreateAtTrait;


    public function __construct()
    {
        $this->carts = new ArrayCollection();
        $this->created_at = new \DateTimeImmutable();
        $this->images = new ArrayCollection();
        $this->bestOffers = new ArrayCollection();
    }

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: "IDENTITY")]
    #[ORM\Column(name: "ItemID", type: "integer", nullable: false)]
    private $itemid;

    #[ORM\Column(name: "Name", type: "string", length: 255, nullable: false)]
    private $name;


    #[ORM\Column(name: "Description", type: "text", length: 65535, nullable: true)]
    private $description;

    #[ORM\Column(name: "Video", type: "string", length: 255, nullable: true)]
    private $video;


    #[ORM\Column(name: "BuyItNowPrice", type: "decimal", precision: 10, scale: 2, nullable: true)]
    private $buyitnowprice;


    #[ORM\Column(name: "IsSold", type: "boolean", nullable: true)]
    private $issold = '0';


    #[ORM\ManyToMany(targetEntity: "ShoppingCart", mappedBy: "items")]
    private $carts = array();

    #[ORM\ManyToOne(inversedBy: 'items')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Category $Category = null;

    #[ORM\OneToMany(mappedBy: 'item', targetEntity: Image::class, cascade: ['persist'], orphanRemoval: true)]
    private Collection $images;

    #[ORM\OneToOne(mappedBy: 'Item', cascade: ['persist', 'remove'])]
    private ?Auction $auction = null;

//    cascade  remove
    #[ORM\OneToMany(mappedBy: 'item', targetEntity: BestOffer::class, cascade: ['remove'])]
    private Collection $bestOffers;

    #[ORM\ManyToOne(inversedBy: 'items')]
    #[ORM\JoinColumn(name: 'sellerID', referencedColumnName: 'id')]
    private ?User $seller = null;

    #[ORM\ManyToOne(inversedBy: 'items')]
    #[ORM\JoinColumn(nullable: false)]
    private ?SellingCategory $sellingCategory = null;

    public function getItemid(): ?int
    {
        return $this->itemid;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): static
    {
        $this->description = $description;

        return $this;
    }

    public function getVideo(): ?string
    {
        return $this->video;
    }

    public function setVideo(?string $video): static
    {
        $this->video = $video;

        return $this;
    }


    public function getBuyitnowprice(): ?string
    {
        return $this->buyitnowprice;
    }

    public function setBuyitnowprice(?string $buyitnowprice): static
    {
        $this->buyitnowprice = $buyitnowprice;

        return $this;
    }

    public function getAuctionendtime(): ?\DateTimeInterface
    {
        return $this->auctionendtime;
    }

    public function setAuctionendtime(?\DateTimeInterface $auctionendtime): static
    {
        $this->auctionendtime = $auctionendtime;

        return $this;
    }

    public function isIssold(): ?bool
    {
        return $this->issold;
    }

    public function setIssold(?bool $issold): static
    {
        $this->issold = $issold;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->Category;
    }

    public function setCategory(?Category $Category): static
    {
        $this->Category = $Category;

        return $this;
    }

    /**
     * @return Collection<int, ShoppingCart>
     */
    public function getCarts(): Collection
    {
        return $this->carts;
    }

    public function addCartid(ShoppingCart $cartid): static
    {
        if (!$this->carts->contains($cartid)) {
            $this->carts->add($cartid);
            $cartid->addItemid($this);
        }

        return $this;
    }

    public function removeCartid(ShoppingCart $cartid): static
    {
        if ($this->carts->removeElement($cartid)) {
            $cartid->removeItemid($this);
        }

        return $this;
    }

    public function addCart(ShoppingCart $cart): static
    {
        if (!$this->carts->contains($cart)) {
            $this->carts->add($cart);
            $cart->addItem($this);
        }

        return $this;
    }

    public function removeCart(ShoppingCart $cart): static
    {
        if ($this->carts->removeElement($cart)) {
            $cart->removeItem($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, Image>
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    public function addImage(Image $image): static
    {
        if (!$this->images->contains($image)) {
            $this->images->add($image);
            $image->setItem($this);
        }

        return $this;
    }

    public function removeImage(Image $image): static
    {
        if ($this->images->removeElement($image)) {
            // set the owning side to null (unless already changed)
            if ($image->getItem() === $this) {
                $image->setItem(null);
            }
        }

        return $this;
    }

    public function getAuction(): ?Auction
    {
        return $this->auction;
    }

    public function setAuction(?Auction $auction): static
    {
        // unset the owning side of the relation if necessary
        if ($auction === null && $this->auction !== null) {
            $this->auction->setItem(null);
        }

        // set the owning side of the relation if necessary
        if ($auction !== null && $auction->getItem() !== $this) {
            $auction->setItem($this);
        }

        $this->auction = $auction;

        return $this;
    }

    /**
     * @return Collection<int, BestOffer>
     */
    public function getBestOffers(): Collection
    {
        return $this->bestOffers;
    }

    public function addBestOffer(BestOffer $bestOffer): static
    {
        if (!$this->bestOffers->contains($bestOffer)) {
            $this->bestOffers->add($bestOffer);
            $bestOffer->setItem($this);
        }

        return $this;
    }

    public function removeBestOffer(BestOffer $bestOffer): static
    {
        if ($this->bestOffers->removeElement($bestOffer)) {
            // set the owning side to null (unless already changed)
            if ($bestOffer->getItem() === $this) {
                $bestOffer->setItem(null);
            }
        }

        return $this;
    }

    public function getSeller(): ?User
    {
        return $this->seller;
    }

    public function setSeller(?User $seller): static
    {
        $this->seller = $seller;

        return $this;
    }

    public function getSellingCategory(): ?SellingCategory
    {
        return $this->sellingCategory;
    }

    public function setSellingCategory(?SellingCategory $sellingCategory): static
    {
        $this->sellingCategory = $sellingCategory;

        return $this;
    }


}
