<?php

namespace App\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table(name: 'Offer', schema: '', indexes: [
    new ORM\Index(name: 'BuyerID', columns: ['BuyerID']),
    new ORM\Index(name: 'ItemID', columns: ['ItemID'])
])]
class Offer
{
    #[ORM\Id]
    #[ORM\Column(name: 'OfferID', type: 'integer', nullable: false)]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private int $offerid;

    #[ORM\Column(name: 'OfferAmount', type: 'decimal', precision: 10, scale: 2, nullable: true)]
    private ?string $offeramount;

    #[ORM\Column(name: 'OfferTime', type: 'datetime', nullable: true)]
    private ?\DateTimeInterface $offertime = null;

    #[ORM\Column(name: 'IsAccepted', type: 'boolean', nullable: true)]
    private ?bool $isaccepted = false;

    #[ORM\ManyToOne(targetEntity: Item::class, inversedBy:  "bestOffers")]
    #[ORM\JoinColumn(name: 'ItemID', referencedColumnName: 'ItemID', onDelete: 'CASCADE')]
    private ?Item $item;

    #[ORM\ManyToOne(targetEntity: User::class)]
    #[ORM\JoinColumn(name: 'BuyerID', referencedColumnName: 'id')]
    private ?User $buyer;

    public function getOfferid(): ?int
    {
        return $this->offerid;
    }

    public function getOfferamount(): ?string
    {
        return $this->offeramount;
    }

    public function setOfferamount(?string $offeramount): static
    {
        $this->offeramount = $offeramount;

        return $this;
    }

    public function getOffertime(): ?\DateTimeInterface
    {
        return $this->offertime;
    }

    public function setOffertime(?\DateTimeInterface $offertime): static
    {
        $this->offertime = $offertime;

        return $this;
    }

    public function isIsaccepted(): ?bool
    {
        return $this->isaccepted;
    }

    public function setIsaccepted(?bool $isaccepted): static
    {
        $this->isaccepted = $isaccepted;

        return $this;
    }

    public function getItem(): ?Item
    {
        return $this->item;
    }

    public function setItem(?Item $item): static
    {
        $this->item = $item;

        return $this;
    }

    public function getBuyer(): ?User
    {
        return $this->buyer;
    }

    public function setBuyer(?User $buyer): static
    {
        $this->buyer = $buyer;

        return $this;
    }


}
