<?php

namespace App\Entity;

use App\Entity\Traits\CreateAtTrait;
use App\Repository\UserRepository;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[UniqueEntity(fields: ['email'], message: 'There is already an account with this email')]
#[ORM\Table(name: 'User')]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    use CreateAtTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;
    #[ORM\Column(length: 180, unique: true)]
    private ?string $email = null;
    #[ORM\Column]
    private array $roles = [];
    /**
     * @var string The hashed password
     */
    #[ORM\Column]
    private ?string $password = null;
    #[ORM\Column(length: 50)]
    private ?string $Firstname = null;
    #[ORM\Column(length: 50)]
    private ?string $Prename = null;
    #[ORM\Column(length: 20, nullable: true)]
    private ?string $PhoneNumber = null;
    #[ORM\Column(type: 'boolean')]
    private $isVerified = false;
    #[ORM\OneToMany(mappedBy: 'lastestBidder', targetEntity: Auction::class)]
    private Collection $winningAuction;
    #[ORM\OneToMany(mappedBy: 'seller', targetEntity: Auction::class, orphanRemoval: true)]
    private Collection $auctions;
    #[ORM\OneToOne(mappedBy: 'user', cascade: ['persist', 'remove'])]
    private ?Payment $payment = null;
    #[ORM\OneToMany(mappedBy: 'seller', targetEntity: Item::class)]
    private Collection $items;
    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    private ?Address $address = null;


    public function __construct()
    {
        $this->created_at = new DateTimeImmutable();
        $this->winningAuction = new ArrayCollection();
        $this->auctions = new ArrayCollection();
        $this->items = new ArrayCollection();

    }

    public function getFullname(): string
    {
        return $this->getFirstname() . ' ' . $this->getPrename();
    }
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): static
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string)$this->email;
    }

    public function getRoles(): array
    {
        return $this->roles;
    }

    public function setRoles(array $roles): static
    {
        $this->roles = $roles;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): static
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials(): void
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getFirstname(): ?string
    {
        return $this->Firstname;
    }

    public function setFirstname(string $Firstname): static
    {
        $this->Firstname = $Firstname;

        return $this;
    }

    public function getPrename(): ?string
    {
        return $this->Prename;
    }

    public function setPrename(string $Prename): static
    {
        $this->Prename = $Prename;

        return $this;
    }






    public function getPostalCode(): ?string
    {
        return $this->PostalCode;
    }

    public function setPostalCode(?string $PostalCode): static
    {
        $this->PostalCode = $PostalCode;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->PhoneNumber;
    }

    public function setPhoneNumber(?string $PhoneNumber): static
    {
        $this->PhoneNumber = $PhoneNumber;

        return $this;
    }

    public function isVerified(): bool
    {
        return $this->isVerified;
    }

    public function setIsVerified(bool $isVerified): static
    {
        $this->isVerified = $isVerified;

        return $this;
    }

    /**
     * @return Collection<int, Auction>
     */
    public function getAuctions(): Collection
    {
        return $this->auctions;
    }

    public function addAuction(Auction $auction): static
    {
        if (!$this->auctions->contains($auction)) {
            $this->auctions->add($auction);
            $auction->setSeller($this);
        }

        return $this;
    }

    public function removeAuction(Auction $auction): static
    {
        if ($this->auctions->removeElement($auction)) {
            // set the owning side to null (unless already changed)
            if ($auction->getSeller() === $this) {
                $auction->setSeller(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Auction>
     */
    public function getWinningAuction(): Collection
    {
        return $this->winningAuction;
    }

    public function addWinningAuction(Auction $winningAuction): static
    {
        if (!$this->winningAuction->contains($winningAuction)) {
            $this->winningAuction->add($winningAuction);
            $winningAuction->setLastestBidder($this);
        }

        return $this;
    }

    public function removeWinningAuction(Auction $winningAuction): static
    {
        if ($this->winningAuction->removeElement($winningAuction)) {
            // set the owning side to null (unless already changed)
            if ($winningAuction->getLastestBidder() === $this) {
                $winningAuction->setLastestBidder(null);
            }
        }

        return $this;
    }

    public function isIsVerified(): ?bool
    {
        return $this->isVerified;
    }

    public function getPayment(): ?Payment
    {
        return $this->payment;
    }

    public function setPayment(?Payment $payment): static
    {
        // unset the owning side of the relation if necessary
        if ($payment === null && $this->payment !== null) {
            $this->payment->setUser(null);
        }

        // set the owning side of the relation if necessary
        if ($payment !== null && $payment->getUser() !== $this) {
            $payment->setUser($this);
        }

        $this->payment = $payment;

        return $this;
    }

    /**
     * @return Collection<int, Item>
     */
    public function getItems(): Collection
    {
        return $this->items;
    }

    public function addItem(Item $item): static
    {
        if (!$this->items->contains($item)) {
            $this->items->add($item);
            $item->setSeller($this);
        }

        return $this;
    }

    public function removeItem(Item $item): static
    {
        if ($this->items->removeElement($item)) {
            // set the owning side to null (unless already changed)
            if ($item->getSeller() === $this) {
                $item->setSeller(null);
            }
        }

        return $this;
    }

    public function getAddress(): ?Address
    {
        return $this->address;
    }

    public function setAddress(?Address $address): static
    {
        $this->address = $address;

        return $this;
    }


}