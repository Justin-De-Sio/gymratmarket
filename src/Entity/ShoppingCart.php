<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table(name: 'ShoppingCart', schema: '', indexes: [
    new ORM\Index(name: 'BuyerID', columns: ['BuyerID'])
])]
class ShoppingCart
{
    #[ORM\Id]
    #[ORM\Column(name: 'CartID', type: 'integer', nullable: false)]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private int $cartid;

    #[ORM\ManyToOne(targetEntity: User::class)]
    #[ORM\JoinColumn(name: 'BuyerID', referencedColumnName: 'id')]
    private ?User $buyer;

    #[ORM\ManyToMany(targetEntity: Item::class, inversedBy: 'carts')]
    #[ORM\JoinTable(name: 'ShoppingCartItem',
        joinColumns: [new ORM\JoinColumn(name: 'CartID', referencedColumnName: 'CartID')],
        inverseJoinColumns: [new ORM\JoinColumn(name: 'ItemID', referencedColumnName: 'ItemID')]
    )]
    private Collection $items;

    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    public function getCartid(): ?int
    {
        return $this->cartid;
    }

    public function getBuyer(): ?User
    {
        return $this->buyer;
    }

    public function setBuyer(?User $buyer): static
    {
        $this->buyer = $buyer;

        return $this;
    }

    /**
     * @return Collection<int, Item>
     */
    public function getItems(): Collection
    {
        return $this->items;
    }

    public function addItem(Item $item): static
    {
        if (!$this->items->contains($item)) {
            $this->items->add($item);
        }

        return $this;
    }

    public function removeItem(Item $item): static
    {
        $this->items->removeElement($item);

        return $this;
    }
}
