<?php

namespace App\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table(name: 'Purchase', schema: '', indexes: [
    new ORM\Index(name: 'BuyerID', columns: ['BuyerID']),
    new ORM\Index(name: 'ItemID', columns: ['ItemID'])
])]
class Purchase
{
    #[ORM\Id]
    #[ORM\Column(name: 'PurchaseID', type: 'integer', nullable: false)]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private int $purchaseid;

    #[ORM\Column(name: 'PurchaseTime', type: 'datetime', nullable: true, options: ['default' => 'CURRENT_TIMESTAMP'])]
    private ?\DateTimeInterface $purchasetime = null;

    #[ORM\Column(name: 'FinalPrice', type: 'decimal', precision: 10, scale: 2, nullable: false)]
    private string $finalprice;

    #[ORM\ManyToOne(targetEntity: Item::class)]
    #[ORM\JoinColumn(name: 'ItemID', referencedColumnName: 'ItemID', onDelete: 'CASCADE')]
    private ?Item $item;

    #[ORM\ManyToOne(targetEntity: User::class)]
    #[ORM\JoinColumn(name: 'BuyerID', referencedColumnName: 'id')]
    private ?User $buyer;

    public function getPurchaseid(): ?int
    {
        return $this->purchaseid;
    }

    public function getPurchasetime(): ?\DateTimeInterface
    {
        return $this->purchasetime;
    }

    public function setPurchasetime(?\DateTimeInterface $purchasetime): static
    {
        $this->purchasetime = $purchasetime;

        return $this;
    }

    public function getFinalprice(): ?string
    {
        return $this->finalprice;
    }

    public function setFinalprice(string $finalprice): static
    {
        $this->finalprice = $finalprice;

        return $this;
    }

    public function getItem(): ?Item
    {
        return $this->item;
    }

    public function setItem(?Item $item): static
    {
        $this->item = $item;

        return $this;
    }

    public function getBuyer(): ?User
    {
        return $this->buyer;
    }

    public function setBuyer(?User $buyer): static
    {
        $this->buyer = $buyer;

        return $this;
    }
}
