<?php

namespace App\Entity;

use App\Repository\AuctionRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: AuctionRepository::class)]
#[ORM\Table(name: 'Auction')]
class Auction
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 10, scale: '0')]
    private ?string $startPrice = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 10, scale: 2, nullable: true)]
    private ?string $CurrentPrice = null;

    #[ORM\Column(nullable: true)]
    private ?int $BidCount = null;

    #[ORM\Column]
    private ?\DateTimeImmutable $startTime = null;

    #[ORM\Column]
    private ?\DateTimeImmutable $endTime = null;


    #[ORM\OneToOne(inversedBy: 'auction', cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(referencedColumnName: 'ItemID', nullable: false)]
    private ?Item $Item = null;

    #[ORM\ManyToOne(inversedBy: 'winningAuction')]
    private ?User $lastestBidder = null;

    #[ORM\ManyToOne(inversedBy: 'auctions')]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $seller = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStartPrice(): ?string
    {
        return $this->startPrice;
    }

    public function setStartPrice(string $startPrice): static
    {
        $this->startPrice = $startPrice;

        return $this;
    }

    public function getCurrentPrice(): ?string
    {
        return $this->CurrentPrice;
    }

    public function setCurrentPrice(?string $CurrentPrice): static
    {
        $this->CurrentPrice = $CurrentPrice;

        return $this;
    }

    public function getBidCount(): ?int
    {
        return $this->BidCount;
    }

    public function setBidCount(?int $BidCount): static
    {
        $this->BidCount = $BidCount;

        return $this;
    }

    public function getStartTime(): ?\DateTimeImmutable
    {
        return $this->startTime;
    }

    public function setStartTime(\DateTimeImmutable $startTime): static
    {
        $this->startTime = $startTime;

        return $this;
    }

    public function getEndTime(): ?\DateTimeImmutable
    {
        return $this->endTime;
    }

    public function setEndTime(\DateTimeImmutable $endTime): static
    {
        $this->endTime = $endTime;

        return $this;
    }

    public function getItem(): ?Item
    {
        return $this->Item;
    }

    public function setItem(Item $Item): static
    {
        $this->Item = $Item;

        return $this;
    }

    public function getLastestBidder(): ?User
    {
        return $this->lastestBidder;
    }

    public function setLastestBidder(?User $lastestBidder): static
    {
        $this->lastestBidder = $lastestBidder;

        return $this;
    }

    public function getSeller(): ?User
    {
        return $this->seller;
    }

    public function setSeller(?User $seller): static
    {
        $this->seller = $seller;

        return $this;
    }
}
