<?php

namespace App\Entity;

use App\Repository\BestOfferRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: BestOfferRepository::class)]
#[ORM\Table(name: 'BestOffer')]
class BestOffer
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(targetEntity: Item::class, inversedBy: 'bestOffers')]
    #[ORM\JoinColumn(name: "ItemID", referencedColumnName: "ItemID")]
    private ?Item $item = null;

    #[ORM\Column]
    private ?int $offerPrice = null;

    #[ORM\Column]
    private ?\DateTimeImmutable $offerDate = null;

    #[ORM\Column]
    private ?bool $isAccepted = false;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getItem(): ?Item
    {
        return $this->item;
    }

    public function setItem(?Item $item): static
    {
        $this->item = $item;

        return $this;
    }

    public function getOfferPrice(): ?int
    {
        return $this->offerPrice;
    }

    public function setOfferPrice(int $offerPrice): static
    {
        $this->offerPrice = $offerPrice;

        return $this;
    }

    public function getOfferDate(): ?\DateTimeImmutable
    {
        return $this->offerDate;
    }

    public function setOfferDate(\DateTimeImmutable $offerDate): static
    {
        $this->offerDate = $offerDate;

        return $this;
    }

    public function isIsAccepted(): ?bool
    {
        return $this->isAccepted;
    }

    public function setIsAccepted(bool $isAccepted): static
    {
        $this->isAccepted = $isAccepted;

        return $this;
    }


}
