# Étape de Construction
FROM php:8.1-apache as builder



# Installer les dépendances pour la construction
RUN apt-get update && apt-get install -y \
        libicu-dev \
        zlib1g-dev \
        libzip-dev \
        libpng-dev \
        libjpeg62-turbo-dev \
        libfreetype6-dev \
        libwebp-dev \
    && docker-php-ext-configure gd --with-freetype --with-jpeg --with-webp \
    && docker-php-ext-install intl zip pdo_mysql gd


RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer


COPY . /var/www/html
RUN rm -rf /var/www/html/var /var/www/html/vendor

RUN  adduser myuser
RUN chown -R myuser /var/www/html/
USER myuser

RUN composer install --no-dev --optimize-autoloader --no-scripts
RUN composer dump-env prod

# Étape de Production
FROM php:8.1-apache

# Copier les extensions PHP compilées de l'étape de construction
COPY --from=builder /usr/local/lib/php/extensions /usr/local/lib/php/extensions
COPY --from=builder /usr/local/etc/php/conf.d /usr/local/etc/php/conf.d

# Installer les dépendances d'exécution
RUN apt-get update && apt-get install -y  zlib1g libzip4 libpng16-16 libjpeg62-turbo libfreetype6  \
    && rm -rf /var/lib/apt/lists/*

# Activer le module rewrite d'Apache
RUN a2enmod rewrite

# Définir le répertoire de travail
WORKDIR /var/www/html

# Modifier DocumentRoot dans la configuration d'Apache
RUN sed -i 's!/var/www/html!/var/www/html/public!g' /etc/apache2/sites-available/000-default.conf

# Exposer le port 80
EXPOSE 80

# Copier l'application compilée de l'étape de construction
COPY --from=builder /var/www/html /var/www/html


# Exécuter Apache en premier plan
CMD ["apache2-foreground"]
