# Start with the official PHP image with the Apache server
FROM php:8.1-apache

# Set PHP environment variables for displaying errors in development
ENV PHP_ERROR_REPORTING=E_ALL \
    PHP_DISPLAY_ERRORS=1

# Install necessary dependencies
RUN apt-get update && apt-get install -y \
        libicu-dev \
        zlib1g-dev \
        libzip-dev \
        libpng-dev \
        libjpeg62-turbo-dev \
        libfreetype6-dev \
        libwebp-dev \
    && docker-php-ext-configure gd --with-freetype --with-jpeg --with-webp \
    && docker-php-ext-install \
        intl \
        zip \
        pdo_mysql \
        gd \
    && rm -rf /var/lib/apt/lists/* # clean up cache

# Enable Apache's rewrite module
RUN a2enmod rewrite

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Define the working directory
WORKDIR /var/www/html

# Modify the DocumentRoot in Apache's configuration
RUN sed -i 's!/var/www/html!/var/www/html/public!g' /etc/apache2/sites-available/000-default.conf

# Expose port 80
EXPOSE 80

COPY ./script/prepare.sh /usr/local/bin/prepare.sh

# Make the scripts executable
RUN chmod +x /usr/local/bin/prepare.sh

# Change the container's entrypoint
ENTRYPOINT ["/usr/local/bin/prepare.sh"]

# Execute Apache in the foreground
CMD ["apache2-foreground"]
