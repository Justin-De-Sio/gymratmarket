# GymRatMarket 

## Project Overview
GymRatMarket is a cutting-edge e-commerce auction platform, created as part of an academic project in June 2023 at a London university. We've gone beyond the basic requirements by integrating Docker-based containerization, ensuring a consistent development environment and mirroring real-world practices.

**Repository:** [GymRatMarket Repository](https://gitlab.com/Justin-De-Sio/gymratmarket/-/tree/main?ref_type=heads)

## Technical Architecture

### Containerized Services
- **MySQL Database:** Handles auction-related data and user profiles.
- **Apache Web Server:** Uses a PHP-based Apache image for the web application.
- **Composer Service:** Manages PHP application dependencies.
- **PHP Container:** Executes database migration and seeding scripts via Doctrine.

### Volume Management
- **Database Volume:** `database_data:/var/lib/mysql` for MySQL data persistence.
- **Development Volumes:** Synchronizes files between the `Webserver` and `PHP` volumes at `/var/www/html`.

### Networking
- Efficient communication via Docker Compose's default network.
- Ports: Database (3306), Web Server (8080).

### Dockerfile Configurations
- **Apache Dockerfile:** Configures PHP environment with Apache and necessary extensions.
- **Composer Dockerfile:** Uses the official Composer image, optimized for our needs.
- **PHP Dockerfile:** Dedicated to running database operations.

## Installation Guide

### Prerequisites
- Docker Desktop.

### Setup
1. Clone the GymRatMarket repository.
2. Navigate to the project directory in the terminal.
3. Execute **docker-compose build** to build the Docker containers.
4. Launch the containers with **docker-compose up**. 
5. Access the website through http://localhost:8080.


## Website Directory Breakdown
- `public/`: Frontend assets (CSS, JS, images).
- `src/`: Application core (controllers, models).
- `templates/`: UI components (Twig templates).
- `config/`: Application configurations.
- `migrations/`: Database scripts.
- `tests/`: Testing suite.
- `bin/`: Scripts for console and testing.
- `var/`: Logs and cache.
- `vendor/`: Dependencies and libraries.

## Database and Email Testing

### Database Configuration
- **Host:** localhost
- **Port:** 3306
- **Username:** root
- **Password:** dbPassword
- **Database Name:** marketDB


