$(document).ready(function () {
    // This is the callback function that runs once the document is ready (fully loaded)

    function adjustcombleHeight() {
        // This is a function that adjusts the height of the div with class ".comble"

        var registerHeight = $('.table').outerHeight();
        // It first gets the outer height (including padding and border) of the div with class ".table"

        $('.comble').css('height', 1000 - registerHeight + 'px');
        // Then it sets the height of the ".comble" div by subtracting the height of ".table" div from 1000 pixels
    }

    // Call the function to adjust the initial height of the ".comble" div
    adjustcombleHeight();

    // Listen for window resize events to adjust the height in case of changes
    $(window).resize(function () {
        // This is the callback function that runs whenever the window is resized

        adjustcombleHeight();
        // Call the function to adjust the height of the ".comble" div when the window is resized
    });
});
