// Function to filter rows in a table by a data attribute
function filterTableByAttribute(tableID, attribute, value) {
    // Select the table using the given ID
    const table = document.querySelector(`#${tableID}`);
    // Select all rows in the table that have the given data attribute
    const rows = table.querySelectorAll(`tr[data-${attribute}]`);

    // Loop over each row
    rows.forEach((row) => {
        // Split the data attribute's value into an array
        const rowAttributes = row.getAttribute(`data-${attribute}`).split(',');

        // If the row's data attribute value includes the selected value, or the selected value is '*'
        if (rowAttributes.includes(value) || value === '*') {
            // Show the row
            row.style.display = '';
        } else {
            // Otherwise, hide the row
            row.style.display = 'none';
        }
    });
}

// Select all dropdown filters in the document
document.querySelectorAll('.filter-dropdown').forEach((dropdown) => {
    // Add an event listener for when the selected option in a dropdown changes
    dropdown.addEventListener('change', (event) => {
        // Get the selected value from the dropdown
        const selectedValue = event.target.value;
        // Get the ID of the table to filter from the dropdown's data attributes
        const tableID = event.target.dataset.table;
        // Get the data attribute to filter by from the dropdown's data attributes
        const attribute = event.target.dataset.attribute;
            console.log(attribute)
        // Call the function to filter the table
        filterTableByAttribute(tableID, attribute, selectedValue);
    });
});
