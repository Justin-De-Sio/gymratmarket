// Add click event listener to the 'add to cart' button.
// When the button is clicked, an alert message "Item added to cart!" will be displayed.
document.querySelector('.btn-add-cart').addEventListener('click', function () {
    alert('Item added to cart!');
});

// Select all elements with the data attribute '[data-carousel-btn]'
const buttons = document.querySelectorAll('[data-carousel-btn]');

// For each selected button
buttons.forEach(btn => {
    // Add a click event listener
    btn.addEventListener('click', () => {
        // Determine the offset based on the button's dataset property.
        // If the button is for the 'next' slide, offset is 1, otherwise -1 for the 'previous' slide.
        const offset = btn.dataset.carouselButton === 'next' ? 1 : -1

        // Get the slides element in the closest parent '[data-carousel]' element.
        const slides = btn.closest('[data-carousel]').querySelector('[data-slides]')

        // Get the currently active slide.
        const activeSlide = slides.querySelector('[data-active]')

        // Calculate the new index for the slide to display.
        // Add the offset to the current slide's index.
        let newIndex = [...slides.children].indexOf(activeSlide) + offset

        // If the new index is out of bounds (less than 0), set it to the last slide's index.
        if (newIndex < 0) newIndex = slides.children.length - 1

        // If the new index is out of bounds (greater than or equal to the number of slides), set it to the first slide's index (0).
        if (newIndex >= slides.children.length) newIndex = 0

        // Set the new slide to active
        slides.children[newIndex].dataset.active = true

        // Remove the 'active' dataset property from the previously active slide.
        delete activeSlide.dataset.active
    });
});
