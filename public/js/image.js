let links = document.querySelectorAll("[data-delete]");
// Selects all elements with the data attribute 'data-delete'.

for(let link of links){
    // Loops over each of the selected elements.

    link.addEventListener("click", function(e){
        // Adds a click event listener to each selected element.

        e.preventDefault();
        // Prevents the default click event action from occurring (e.g., following a hyperlink).

        if(confirm("Voulez-vous supprimer cette image ?")){
            // Prompts the user to confirm they want to delete the image.

            fetch(this.getAttribute("href"), {
                // Sends a fetch request to the href of the clicked element (i.e., the 'delete' URL).

                method: "DELETE",
                // Specifies the HTTP method to be used when sending the request. In this case, DELETE.

                headers: {
                    "X-Requested-With": "XMLHttpRequest",
                    "Content-Type": "application/json"
                },
                // Sets the headers of the request.

                body: JSON.stringify({"_token": this.dataset.token})
                // Sets the body of the request, including a token for CSRF protection.
            }).then(response => response.json())
                // Parses the response as JSON.

                .then(data => {
                    // Handles the JSON response.

                    if(data.success){
                        // If the response indicates a successful deletion:

                        this.parentElement.remove();
                        // Removes the parent element of the clicked link from the DOM.
                    }else{
                        // If the deletion was not successful:

                        alert(data.error);
                        // Alerts the user to the error.
                    }
                })
        }
    });
}
