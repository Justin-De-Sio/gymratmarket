// Access the hamburger menu and the navigation links by their class name
const menuHamburger = document.querySelector(".menu-hamburger")
const navLinks = document.querySelector(".nav-links")

// Add a click event listener to the hamburger menu
menuHamburger.addEventListener('click', () => {
    // On clicking the hamburger menu, toggle the 'mobile-menu' class on the navigation links.
    // This will typically show or hide the navigation links on mobile devices, depending on your CSS.
    navLinks.classList.toggle('mobile-menu')
})

// Declare a parallax effect function
function EasyPeasyParallax() {
    // Get the current scroll position of the window
    scrollPos = $(this).scrollTop();

    // Apply a parallax effect on the 'therock' class elements. As you scroll down,
    // these elements will move downwards at a slower speed (i.e., at half of the scrolling speed)
    $('.therock').css({
        'transform': 'translateY(' + (scrollPos / 2.5) + 'px)'
    });

    // Apply a parallax effect on the 'slogant' class elements. As you scroll down,
    // these elements will move downwards and their opacity will decrease
    $('.slogant').css({
        'margin-top': (scrollPos / 2) + 'px',
        'opacity': 2.6 - (scrollPos / 250)
    });
}

// Add a scroll event listener to the window
$(window).scroll(function() {
    // Call the parallax function each time the user scrolls the page
    EasyPeasyParallax();
});
