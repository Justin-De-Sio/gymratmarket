document.addEventListener('change', (event) => {
    var sellingCategory = document.getElementById('item_sellingCategory');
    var buyItNowPriceRow = document.getElementById('item_buyItNowPrice');
    const buyItNowOptionValue = 'buy It Now';  // change this to the actual value
    console.log(sellingCategory.value)
    console.log(buyItNowPriceRow)
    // Function to toggle buyItNowPrice field
    const toggleBuyItNowPrice = () => {
        if (sellingCategory.value === buyItNowOptionValue) {
            buyItNowPriceRow.style.display = 'block';
        } else {
            buyItNowPriceRow.style.display = 'none';
        }
    };

    // Toggle buyItNowPrice field on page load
    toggleBuyItNowPrice();

    // Toggle buyItNowPrice field whenever sellingCategory changes
    sellingCategory.addEventListener('change', toggleBuyItNowPrice);
});

