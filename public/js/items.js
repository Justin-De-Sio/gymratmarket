// Select the element with class name '.button'
const btn = document.querySelector(".button")

// Select the element with class name '.left-filter'
const fil = document.querySelector(".left-filter")

// Add click event listener to 'btn'
btn.addEventListener('click', () => {
    // Toggle the 'mobile-menu' class on 'fil'
    // If 'fil' has the 'mobile-menu' class, it will be removed; if it does not have it, the class will be added.
    fil.classList.toggle('mobile-menu')
})
